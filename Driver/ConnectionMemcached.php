<?php

/**
 * This File is part of the Stream\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Cache\Driver;
use \Memcached, \RuntimeException;

/**
 * Class ConnectionMemcached
 *
 * @package Stream\Cache
 * @version 1.0
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class ConnectionMemcached
{
    /**
     * memcached
     *
     * @var Memcached
     * @access private
     */
    private $memcached;

    /**
     * __construct
     *
     * @param Memcached $memcached
     * @access public
     * @return void
     */
    public function __construct(Memcached $memcached)
    {
        $this->memcached = $memcached;
    }

    /**
     * init
     *
     * @param array $servers
     * @access public
     * @throws RuntimeException
     * @return Memcached
     */
    public function init(array $servers)
    {
        foreach ($servers as $server) {
            $this->memcached->addServer($server['host'], $server['port'], $server['weight']);
        }

        if ($this->memcached->getVersion() === false) {
            throw new RuntimeException('Cannot initialize memcache');
        }

        return $this->memcached;
    }
}
