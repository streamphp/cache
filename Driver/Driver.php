<?php

/**
 * This File is part of package name
 *
 * (c) author <email>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */
namespace Stream\Cache\Driver;
use Stream\Cache\InterfaceCacheDriver as InterfaceDriver;
use \Closure;

/**
 * Class Driver
 * @author
 */

abstract class Driver implements InterfaceDriver
{

    /**
     * default cached expiry time in minutes
     *
     * @var float
     * @access public
     */
    protected $default = 60;

    /**
     * itemExists
     *
     * @param Mixed $cacheid
     * @abstract
     * @access public
     * @return Booelan
     */
    abstract public function cachedItemExists($cacheid);

    /**
     * getCachedItem
     *
     * @param Mixed $cacheid
     * @abstract
     * @access public
     * @return Boolean
     */
    abstract public function getFromCache($cacheid);

    /**
     * writeToCache
     *
     * @param String $cacheid the cache item identifier
     * @param Mixed $data Data to be cached
     * @param Mixed $expires Integer value of the expiry time in minutes or
     * unix timestamp
     * @param boolean $compressed  compress data
     * @abstract
     * @access public
     * @return Boolean
     */
    abstract public function writeToCache($cacheid, $data, $expires = 60, $compressed);

    /**
     * deleteFromCache
     *
     * @param String $cacheid the cache item identifier
     * @abstract
     * @access public
     * @return Boolean
     */
    abstract public function deleteFromCache($cacheid);

    /**
     * flushCache
     *
     * @abstract
     * @access public
     * @return Boolean
     */
    abstract public function flushCache();

    /**
     * saveForeaver
     *
     * @param String $cacheid the cache item identifier
     * @param Mixed $data Data to be cached
     * @param boolean $compressed  compress data
     * @abstract
     * @access public
     * @return Boolean
     */
    abstract public function saveForever($cacheid, $data, $compressed);

    /**
     * test
     *
     * @access public
     * @return boolean
     */
    public function test()
    {
        return true;
    }

    /**
     * get default expiry time
     */
    public function getDefaultExpiry()
    {
        return $this->default;
    }
}

