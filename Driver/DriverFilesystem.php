<?php

/**
 * This File is part of the Stream\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Cache\Driver;

use Stream\Filesystem\FSDirectory;

/**
 * DriverFileSystem
 *
 * @uses Storage
 * @package Stream\Cache
 * @version 1.0
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class DriverFilesystem extends Driver
{
    /**
     * Flag a file as compressed
     */
    const C_COMPRESSED = 1;

    /**
     *
     * Flag a file as uncompressed
     */
    const C_UNCOMPRESSED = 0;

    /**
     * cache directory
     *
     * @var Stream\FSDirectory
     * @access protected
     */
    protected $cachedir;

    /**
     * serializableObjects
     *
     * @var Mixed
     * @access protected
     */
    protected static $serializableObjects;

    /**
     * __construct
     *
     * @param FSDirectory  $cachedir  cache directory
     * @access public
     * @return void
     */
    public function __construct(FSDirectory $cachedir)
    {
        $this->cachedir = $cachedir;
        $this->setSerializer();
    }

    /**
     * cachedItemExists
     *
     * @param Mixed $cacheid
     * @access protected
     * @return void
     */
    public function cachedItemExists($cacheid)
    {
        if (!$file = $this->cachedir->get($cacheid)) {
            return false;
        }
        $timestamp = (int)$file->contents(0, 10);

        if (time() < $timestamp) {
            return true;
        }

        return false;
    }

    /**
     * getFromCache
     *
     * @param Mixed $cacheid
     * @access protected
     * @return Mixed
     */
    public function getFromCache($cacheid)
    {
        if (!$file = $this->cachedir[$cacheid]) {
            return;
        }

        $contents = $file->contents();
        $state = (int)substr($contents, 11, 1);
        $data = substr($contents, 13);

        if ($state === self::C_UNCOMPRESSED) {
            return unserialize($data);
        }

        return unserialize($this->uncompressData($data));
    }


    /**
     * saveForever
     *
     * @param Mixed $cacheid
     * @param Mixed $data
     * @param Mixed $compressed
     * @access protected
     * @return Booelan
     */
    public function saveForever($cacheid, $data, $compressed)
    {
        return $this->writeToCache($cacheid, $data, "2037-12-31", $compressed);
    }

    /**
     * deleteFromCache
     *
     * @param Mixed $cacheid
     * @access protected
     * @return Boolean
     */
    public function deleteFromCache($cacheid)
    {
        unset($this->cachedir[$cacheid]);
        return true;
    }

    /**
     * flushCache
     *
     * @access protected
     * @return Boolean
     */
    public function flushCache()
    {
        $this->cachedir->purgeFiles();
        return true;
    }

    /**
     * writeToCache
     *
     * @todo test without igbinary
     *
     * @param Mixed $cacheid
     * @param Mixed $data
     * @param int $expires
     * @param Mixed $compressed
     * @access protected
     * @return Boolean
     */
    public function writeToCache($cacheid, $data, $expires = 1, $compressed)
    {
        $data = $this->serializeWithTime($data, $compressed);

        if (isset($this->cachedir[$cacheid])) {
            unset($this->cachedir[$cacheid]);
        }

        if ($this->cachedir->touch($cacheid)) {
            $file = $this->cachedir->get($cacheid);
            $file->put($data, 0644);
            return true;
        }

        return false;
    }

    /**
     * setSerializer
     *
     */
    protected function setSerializer()
    {
        if (is_null(static::$serializableObjects)) {
            static::$serializableObjects = $this->canSerializeObjects();
        }
    }

    /**
     * compressData
     *
     * @param Mixed $data
     * @access private
     * @return String base64 string representation of gzip compressed input
     * data
     */
    private function compressData($data)
    {
        return base64_encode(gzcompress($data));
    }

    /**
     * uncompressData
     *
     * @param Mixed $data
     * @access private
     * @return String Mixed contents of the cached item
     */
    private function uncompressData($data)
    {
        return gzuncompress(base64_decode($data));
    }

    /**
     * serializeWithTime
     *
     * @param Mixed $data
     * @param Mixed $time
     * @param Mixed $compressed
     * @access private
     * @return Mixed file contents
     */
    private function serializeWithTime($data, $time, $compressed = false)
    {
        $timestamp = is_int($time) ? time() +  ($time * 60) : (is_string($time) ? strtotime($time) : (time() + $this->default * 60));
        $data = $compressed ? $this->compressData($data) : $data;
        $contents = sprintf('%s;%d;%s', $timestamp, $compressed ? self::C_COMPRESSED : self::C_UNCOMPRESSED, serialize($data));
        return $contents;
    }

    /**
     * canSerializeObjects
     *
     * @access private
     * @return Boolean
     */
    private function canSerializeObjects()
    {
        return extension_loaded('igbinary');
    }

}
