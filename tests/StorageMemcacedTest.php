<?php

/**
 * This File is part of the Stream\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Cache;

use Stream\Cache\Driver\ConnectionMemcached;
use Stream\Cache\Storage;
use Stream\Cache\Driver\DriverMemcached;
use \Memcached;


/**
 * Class: StorageMemcachedTest
 *
 * @see \TestCases
 */
class StorageMemcachedTest extends StorageTestCases
{

    protected $memcached;

    /**
     * setUp
     *
     * @access protected
     * @return void
     */
    protected function setUp()
    {
        $connection = new ConnectionMemcached(new \Memcached('fooish'));

        $this->memcached = $connection->init(array(
            array(
                'host' => '127.0.0.1',
                'port' => 11211,
                'weight' => 100
            )
        ));
        $driver = new DriverMemcached($this->memcached);
        $this->cache = new Storage($driver, 'mycache');
    }

    /**
     * tearDown
     *
     * @access protected
     * @return void
     */
    protected function tearDown()
    {
        //$this->object->purge();
    }
}
