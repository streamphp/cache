<?php

/**
 * This File is part of Stream\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */
namespace Stream\Cache;

use ArrayAccess, Closure;
use Stream\Cache\InterfaceCache  as InterfaceCache;
use Stream\Cache\InterfaceCacheDriver as InterfaceCacheDriver;

/**
 * Storage
 *
 * @uses ArrayAccess
 * @package Stream\Cache
 * @version 1.0
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class Storage implements InterfaceCache, ArrayAccess
{
    /**
     * cache driver instance
     *
     * @var Stream\Cache\Interfaces\Driver
     */
    protected $cacheDriver;

    /**
     * Array containing already retrieved items from the caching source
     *
     * @var array
     * @access protected
     */
    protected $pool = [];

    /**
     * cache id prefix
     *
     * @var string
     * @access public
     */
    protected $prefix;

    /**
     * __construct
     *
     * @param Stream\Cache\Interfaces\Driver $cachDriver
     * @param string $prefix cache id prefix
     */
    public function __construct(InterfaceCacheDriver $cachDriver, $prefix = 'cache')
    {
        $this->cacheDriver = $cachDriver;
        $this->setCachePrefix($prefix);
    }

    /**
     * Check if an item is already cached
     *
     * @param String $cacheid the cache item identifier
     * @access public
     * @return Boolean
     */
    public function has($cacheid)
    {
        $cacheid = $this->getCacheID($cacheid);

        if (isset($this->pool[$cacheid])) {
            return true;
        }

        return $this->cacheDriver->cachedItemExists($cacheid);
    }

    /**
     * Retrieves an Item from cache
     *
     * @param String $cacheid the cache item identifier
     * @access public
     * @return Mixed the cached data or null
     */
    public function read($cacheid)
    {
        $cacheid = $this->getCacheID($cacheid);

        if (isset($this->pool[$cacheid])) {
            return $this->pool[$cacheid];
        }

        if ($data = $this->cacheDriver->getFromCache($cacheid)) {
            return $data;
        }

        return null;
    }

    /**
     * Writes an item to cache
     *
     * @param String $cacheid the cache item identifier
     * @param Mixed $data Data to be cached
     * @param Mixed $expires Integer value of the expiry time in minutes or
     * unix timestamp
     * @param Boolean $compressed weather the data should be compresse or not
     * @access public
     * @return Boolean
     */
    public function write($cacheid, $data, $expires = null, $compressed = false)
    {
        $cacheid = $this->getCacheID($cacheid);

        if (is_null($expires)) {
            $expires = $this->cacheDriver->getDefaultExpiry();
        }

        if ($this->cacheDriver->writeToCache($cacheid, $data, $expires, $compressed)) {
            $this->pool[$cacheid] = $this->cacheDriver->getFromCache($cacheid);
            return true;
        }

        return false;
    }

    /**
     * Save an item to cache with a far future expiry time (forever)
     *
     * @param string  $cacheid     the cache item identifier
     * @param mixed   $data        Data to be cached
     * @param boolean $compressed  compress data
     * @access public
     * @return boolean
     */
    public function seal($cacheid, $data, $compressed = false)
    {
        $cacheid = $this->getCacheID($cacheid);
        return $this->write($cacheid, $data, 0, $compressed);
    }

    /**
     * Writes default data to cache.
     *
     * @param String $cacheid the cache item identifier
     * @param Mixed $expires Integer value of the expiry time in minutes or
     * unix timestamp
     * @param Closure $callback A callback function that returns default data
     * @param boolean $compressed  compress data
     * @access public
     * @return Mixed the cached item or results of the callback
     */
    public function writeDefault($cacheid, $expires = null, Closure $callback, $compressed = false)
    {
        $this->write($cacheid, $default = $this->execDefaultVal($cacheid, $callback), $expires, $compressed);
        return $default;

    }

    /**
     * Writes default data to cache with a far future expiry date.
     *
     * @param String $cacheid the cache item identifier
     * @param Closure $callback A callbacl function that returns default data
     * @param boolean $compressed  compress data
     * @access public
     * @return Mixed the cached item or results of the callback
     */
    public function sealDefault($cacheid, Closure $callback, $compressed = false)
    {
        $this->seal($cacheid, $default = $this->execDefaultVal($cacheid, $callback), $compressed);
        return $default;
    }

    /**
     * Delete an item from cache
     *
     * If no cache id is specified, the cache will be flushed.
     *
     * @param String $cacheid
     * @access public
     * @return Boolena
     */
    public function purge($cacheid = null)
    {

        if (is_null($cacheid)) {
            $this->pool = [];
            return $this->cacheDriver->flushCache();
        }

        $cacheid = $this->getCacheID($cacheid);

        if ($deleted = $this->cacheDriver->deleteFromCache($cacheid)) {
            unset($this->pool[$cacheid]);
            return $deleted;
        }

        return false;;
    }
    /**
     * offsetExists
     *
     * @param Mixed $offset
     * @access public
     * @return void
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    /**
     * offsetUnset
     *
     * @param Mixed $offset
     * @access public
     * @return void
     */
    public function offsetUnset($offset)
    {
        return $this->purge($offset);
    }

    /**
     * offsetSet
     *
     * @param Mixed $offset
     * @param Mixed $value
     * @access public
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        return $this->write($offset);
    }

    /**
     * offsetGet
     *
     * @param Mixed $offset
     * @access public
     * @return void
     */
    public function offsetGet($offset)
    {
        return !$this->has($offset) ?: $this->read($offset);
    }

    /**
     * getCacheID
     *
     * @param Mixed $key
     * @access protected
     * @return void
     */
    protected function getCacheID($key)
    {
        return sprintf("%s_%s", $this->prefix, $key);
    }

    /**
     * setCachePrefix
     *
     * @param Mixed $prefix
     * @access protected
     * @return void
     */
    protected function setCachePrefix($prefix = null)
    {
        $this->prefix = is_null($prefix) ? 'cached' : $prefix;
    }

    /**
     * execDefaultVal
     *
     * @param Mixed $cacheid
     * @param Closure $callback
     * @access protected
     * @return void
     */
    protected function execDefaultVal($cacheid, Closure $callback)
    {

        if ($this->has($cacheid)) {
            return $this->read($cacheid);
        }

        return $callback();
    }
}

