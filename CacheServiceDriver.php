<?php

/**
 * This File is part of the Stream\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Cache;

use Stream\Common\AbstractServiceDriver;

/**
 * @class FilesystemServiceDriver
 */

class CacheServiceDriver extends AbstractServiceDriver
{
    /**
     * {@inheritDoc}
     */
    protected function registerService()
    {
        $app = $this->app;

        return [
            'cache' => $this->app->share(
                function () use ($app) {
                    return new Storage();
                }
            )
        ];
    }
}
